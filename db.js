const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2118578","root","password",{host:"127.0.0.1",dialect:"mysql",logging:false});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//import modela
db.vjezba = require('./models/vjezba.js')(sequelize);
db.zadatak = require('./models/zadatak.js')(sequelize);
db.student = require('./models/student.js')(sequelize);
db.grupa = require('./models/grupa.js')(sequelize);

//relacije
// Veza 1-n vise studenata se moze nalaziti u jednoj grupi
// Veza 1-n vise zadataka se moze nalaziti u jednoj vjezbi
db.vjezba.hasMany(db.zadatak,{as:'zadaciVjezbe'});
db.grupa.hasMany(db.student,{as:'studentiGrupe'});

module.exports=db;