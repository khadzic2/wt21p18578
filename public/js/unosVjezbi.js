 var brojZadatakaDiv, brojVjezbiElement,brojVjezbi;
 window.onload = function () {
  	document.getElementById("brojVjezbiInput").addEventListener("change", function(){
		brojZadatakaDiv = document.getElementById("brojZadatakaDiv");
	  	brojVjezbiElement = document.getElementById("brojVjezbiInput");
		brojVjezbi = brojVjezbiElement.value;
		
		VjezbeAjax.dodajInputPolja(brojZadatakaDiv,brojVjezbi);
	});

	document.getElementById("posaljiZadatke").addEventListener("click", function(){
		var vjezbeObjekat = {brojVjezbi: brojVjezbi, brojZadataka:[]};

		for(var i = 0; i < brojVjezbi; i++){
			var brojZadatakaPoVjezbi = document.getElementById("z" + i).value;
			vjezbeObjekat.brojZadataka.push(brojZadatakaPoVjezbi);
		}

		VjezbeAjax.posaljiPodatke(vjezbeObjekat,function (error,data){});
	})
 }