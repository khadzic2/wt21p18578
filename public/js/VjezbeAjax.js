var VjezbeAjax = (function () {
	var pretohdnoKliknuto = null;
	var kreiraniZadaciDivs = [];

	var dodajInputPolja = function (DOMelementDIVauFormi,brojVjezbi) {
		while(DOMelementDIVauFormi.children.length != 0) {
			DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.lastElementChild);
		}

		if(brojVjezbi > 0 && brojVjezbi < 16){
			for(var i = 0; i < brojVjezbi; i++){
				var label = document.createElement("label");
				label.id = "z" + i + "Labela";
				label.appendChild(document.createTextNode("z" + i));
				DOMelementDIVauFormi.appendChild(label);
				var input = document.createElement("input");
				input.id = "z" + i;
				input.name = "z" + i;
				input.value = "4";
				input.type = "number";
				DOMelementDIVauFormi.appendChild(input);
				var br = document.createElement("br");
				DOMelementDIVauFormi.appendChild(br);
			}	
		}else{
			console.log("Broj vjezbi nije u opsegu u kojem treba biti!");
		}
	}

	var provjeriPodatke = function(jsonRez){
		var brojVjezbi = jsonRez.brojVjezbi;
		var brojZadataka = jsonRez.brojZadataka;
		var status = "error";
		var data = "Pogrešan parametar ";
		var greska = false;

		if(brojVjezbi < 1 || brojVjezbi > 15){
			data += "brojVjezbi";
			greska = true;
		}

		for (var i = 0; i < brojZadataka.length; i++) {
			if(brojZadataka[i] < 0 || brojZadataka[i] > 10){
				if(greska) data += ",";

				data += "z" + i;
				greska = true;
			}
		}

		if(brojZadataka.length != brojVjezbi){
			if(greska) data += ",";

			greska = true;
			data += "brojZadataka";
		}

		var objekat = {};
		if(greska) objekat = {status: status, data: data};
		else objekat = {brojVjezbi: brojVjezbi, brojZadataka: brojZadataka};

		return objekat;	
	}

	var posaljiPodatke = function (vjezbeObjekat,callbackFja){
		   var ajax = new XMLHttpRequest();
		   ajax.onreadystatechange = function() {
		        if (ajax.readyState == 4 && ajax.status == 200){
		           var jsonRez = JSON.parse(ajax.responseText);

		          if(jsonRez.status == "error")
			          	callbackFja(jsonRez.data,null);
		          else if(jsonRez.status != "error")
			           	callbackFja(null,jsonRez);
		        }else if(ajax.readyState == 4)
		        	callbackFja(ajax.statusText, null);
		   }
		   ajax.open("POST","http://localhost:3000/vjezbe",true);
		   ajax.setRequestHeader("Content-Type", "application/json");
		   ajax.send(JSON.stringify(vjezbeObjekat));
	}



	var dohvatiPodatke = function (callbackFja){
		   var ajax = new XMLHttpRequest();
		   ajax.onreadystatechange = function() {
		        if (ajax.readyState == 4 && ajax.status == 200){
		           var jsonRez = JSON.parse(ajax.responseText);
		           jsonRez = provjeriPodatke(jsonRez);
		          if(jsonRez.status == "error")
			          	callbackFja(jsonRez.data,null);
		          else if(jsonRez.status != "error")
			           	callbackFja(null,jsonRez);
		        }
		        else if (ajax.readyState == 4)
		      		callbackFja(ajax.statusText,null);
		   }
		   ajax.open("GET","http://localhost:3000/vjezbe",true);
		   ajax.send();
	}

	var iscrtajVjezbe = function (divDOMelement, objekat) {

		for (var i = 0; i < objekat.brojVjezbi; i++) {
			var index = i + 1;

			var divVjezba = document.createElement("div");
			divVjezba.id = "divVjezba" + index.toString();
			divVjezba.classList.add("divVjezba");

			var input = document.createElement("input");
			input.id="buttonVjezba" + index.toString();
			input.classList.add("buttonVjezba");
			input.name="buttonVjezba" + index.toString();
			input.type = "button";
			input.value = "VJEŽBA " + index.toString();

			divVjezba.appendChild(input);
			divDOMelement.appendChild(divVjezba);
			
			function funkcija(divVjezba, indeks){
				input.addEventListener("click",function(){
					iscrtajZadatke(divVjezba,objekat.brojZadataka[indeks]);
				})
			}

			funkcija(divVjezba,i);
		}		
	}

	var iscrtajZadatke = function(vjezbaDOMelement,brojZadataka){
			if(pretohdnoKliknuto != null && pretohdnoKliknuto != vjezbaDOMelement){
				pretohdnoKliknuto.getElementsByTagName("div")[0].style.display = "none";
			}

			var kreiraniZadaciRanije = false;
			for(var i = 0; i < kreiraniZadaciDivs.length; i++){
				if(kreiraniZadaciDivs[i] == vjezbaDOMelement){
					kreiraniZadaciRanije = true;
					break;
				}
			}

			if(kreiraniZadaciRanije){
				vjezbaDOMelement.getElementsByTagName("div")[0].style.display = "grid";
			}else {
				var divZadaci = document.createElement("div");
				divZadaci.classList.add("divZadaci");

				for (var i = 0; i < brojZadataka; i++) {
					
					var input = document.createElement("input");
					input.id="buttonZadatak" + (i+1).toString();
					input.classList.add("buttonZadatak");
					input.name="buttonZadatak" + (i + 1).toString();
					input.type = "button";
					input.value = "ZADATAK " + (i + 1).toString();

					divZadaci.appendChild(input);

				}

				vjezbaDOMelement.appendChild(divZadaci);

				kreiraniZadaciDivs.push(vjezbaDOMelement);
			}

			pretohdnoKliknuto = vjezbaDOMelement;
	}

	return{
		dodajInputPolja: dodajInputPolja,
		posaljiPodatke: posaljiPodatke,
		dohvatiPodatke: dohvatiPodatke,
		iscrtajVjezbe: iscrtajVjezbe,
		iscrtajZadatke: iscrtajZadatke
	}	
}());