
var assert = chai.assert;
chai.should();

describe('VjezbeAjax', function(){
	describe('dodajInputPolja()', function(){
	 	afterEach(function() {
	 		var div = document.getElementById("mocha");

	 		while(div.children.length != 0)
	 			div.removeChild(div.lastElementChild);

	 	});

		it('test1 - provjera kolicine labela i inputa', function(){
			var div = document.getElementById('mocha');

			VjezbeAjax.dodajInputPolja(div,10);

			var brojInputPolja = div.getElementsByTagName("input").length;
			var brojLabelPolja = div.getElementsByTagName("label").length;

			var ocekivano = "10 10";
			var stvarno = brojLabelPolja + " " + brojInputPolja;

			assert.equal(ocekivano,stvarno,"U div treba biti dodano 10 labela i 10 input polja");

		})

		it('test2 - provjera kolicine labela i inputa (0 - nepravilan broj vjezbi)', function(){
			var div = document.getElementById('mocha');

			VjezbeAjax.dodajInputPolja(div,16);

			var brojInputPolja = div.getElementsByTagName("input").length;
			var brojLabelPolja = div.getElementsByTagName("label").length;

			var ocekivano = "0 0";
			var stvarno = brojLabelPolja + " " + brojInputPolja;

			VjezbeAjax.dodajInputPolja(div,0);

			brojInputPolja = div.getElementsByTagName("input").length;
			brojLabelPolja = div.getElementsByTagName("label").length;

			stvarno = brojLabelPolja + " " + brojInputPolja;

			assert.equal(ocekivano,stvarno,"U div ne treba biti dodana nijedna labela a nijedan input");

		})

		it('test3 - provjera defaultnih vrijednosti input polja', function() {
			var div = document.getElementById('mocha');

			VjezbeAjax.dodajInputPolja(div,10);

			var stvarno = "";

			for(var i = 0; i < 10; i++){
				stvarno += 'z' + i + '=' + document.getElementById('z' + i).value;
				if(i != 9) stvarno += ", ";
			}

			var ocekivano = 'z0=4, z1=4, z2=4, z3=4, z4=4, z5=4, z6=4, z7=4, z8=4, z9=4';

			assert.equal(ocekivano,stvarno,"Sva input polja moraju imati defaultnu vrijednost 4");
		})
	})

	describe('posaljiPodatke()', function() {
		beforeEach(function() {
		  this.xhr = sinon.useFakeXMLHttpRequest();
		 
		  this.requests = [];
	      this.xhr.onCreate = function(xhr) {
	      	this.requests.push(xhr);
		  }.bind(this);
		});
		 
		afterEach(function() {
		  this.xhr.restore();
		});

		it('test1 - slanje ispravnih podataka', function() {
			var data = {brojVjezbi: 10, brojZadataka: [1,2,3,4,5,6,7,8,9,10]};
			var dataJson = JSON.stringify(data);

			VjezbeAjax.posaljiPodatke(data, function(error, data) {
				assert.equal(error,null);
				assert.equal(data.brojVjezbi,10);
				assert.equal(data.brojZadataka[0],1);
				assert.equal(data.brojZadataka[1],2);
				assert.equal(data.brojZadataka[2],3);
				assert.equal(data.brojZadataka[3],4);
				assert.equal(data.brojZadataka[4],5);
				assert.equal(data.brojZadataka[5],6);
				assert.equal(data.brojZadataka[6],7);
				assert.equal(data.brojZadataka[7],8);
				assert.equal(data.brojZadataka[8],9);
				assert.equal(data.brojZadataka[9],10);
			})

			this.requests[0].respond(200, { 'Content-Type': 'text/json' }, dataJson);
			this.requests[0].requestBody.should.equal(dataJson);
		})

		it('test2 - slanje neispravnih podataka', function() {
			var data = {brojVjezbi: 10, brojZadataka: [1,2,3,4,5,6,7,8,9]};
			var greska = {status:"error", data:"Pogrešan parametar brojZadataka"};
			var dataJson = JSON.stringify(greska);

			VjezbeAjax.posaljiPodatke(data, function(error, data) {
				assert.equal(error,"Pogrešan parametar brojZadataka","error nije null");
				assert.equal(data,null,"Data mora biti null");
			})

			this.requests[0].respond(200, { 'Content-Type': 'text/json' }, dataJson);
			this.requests[0].requestBody.should.equal(JSON.stringify(data));
		})

		it('test3 - slanje neispravnih podataka', function() {
			var data = {brojVjezbi: 0, brojZadataka: []};
			var greska = {status:"error", data:"Pogrešan parametar brojVjezbi"};
			var dataJson = JSON.stringify(greska);

			VjezbeAjax.posaljiPodatke(data, function(error, data) {
				assert.equal(error,"Pogrešan parametar brojVjezbi","error nije null");
				assert.equal(data,null,"Data mora biti null");
			})

			this.requests[0].respond(200, { 'Content-Type': 'text/json' }, dataJson);
			this.requests[0].requestBody.should.equal(JSON.stringify(data));
		})

		it('test4 - slanje neispravnih podataka', function() {
			var data = {brojVjezbi: 5, brojZadataka: [11,11,11,11,11]};
			var greska = {status:"error", data:"Pogrešan parametar z0,z1,z2,z3,z4"};
			var dataJson = JSON.stringify(greska);

			VjezbeAjax.posaljiPodatke(data, function(error, data) {
				assert.equal(error,"Pogrešan parametar z0,z1,z2,z3,z4","error nije null");
				assert.equal(data,null,"Data mora biti null");
			})

			this.requests[0].respond(200, { 'Content-Type': 'text/json' }, dataJson);
			this.requests[0].requestBody.should.equal(JSON.stringify(data));
		})

		it('test5 - slanje neispravnih podataka', function() {
			var data = {brojVjezbi: 0, brojZadataka: [11,11,11,11,11]};
			var greska = {status:"error", data:"Pogrešan parametar brojVjezbi,z0,z1,z2,z3,z4,brojZadataka"};
			var dataJson = JSON.stringify(greska);

			VjezbeAjax.posaljiPodatke(data, function(error, data) {
				assert.equal(error,"Pogrešan parametar brojVjezbi,z0,z1,z2,z3,z4,brojZadataka","error nije null");
				assert.equal(data,null,"Data mora biti null");
			})

			this.requests[0].respond(200, { 'Content-Type': 'text/json' }, dataJson);
			this.requests[0].requestBody.should.equal(JSON.stringify(data));
		})

	 })

	 describe('dohvatiPodatke()', function (argument) {
		beforeEach(function() {
		  this.xhr = sinon.useFakeXMLHttpRequest();
		 
		  this.requests = [];
	      this.xhr.onCreate = function(xhr) {
	      	this.requests.push(xhr);
		  }.bind(this);
		});
		 
		afterEach(function() {
		  this.xhr.restore();
		});

		it('test1 - dohvacanje ispravnih podataka', function(done) {
			var data = {brojVjezbi: 10, brojZadataka: [1,2,3,4,5,6,7,8,9,10]};
			var dataJson = JSON.stringify(data);

			VjezbeAjax.dohvatiPodatke(function(error, data) {
				assert.equal(error,null);
				assert.equal(data.brojVjezbi,10);
				assert.equal(data.brojZadataka[0],1);
				assert.equal(data.brojZadataka[1],2);
				assert.equal(data.brojZadataka[2],3);
				assert.equal(data.brojZadataka[3],4);
				assert.equal(data.brojZadataka[4],5);
				assert.equal(data.brojZadataka[5],6);
				assert.equal(data.brojZadataka[6],7);
				assert.equal(data.brojZadataka[7],8);
				assert.equal(data.brojZadataka[8],9);
				assert.equal(data.brojZadataka[9],10);

				done();
			})

			this.requests[0].respond(200, { 'Content-Type': 'text/json' }, dataJson);
		})

		it('test2 - dohvacanje neispravnih podataka', function() {
			var data = {brojVjezbi: 11, brojZadataka: [1,2,3,4,5,6,7,8,9,10]};
			var dataJson = JSON.stringify(data);

			VjezbeAjax.dohvatiPodatke(function(error, data) {
				assert.equal(error,'Pogrešan parametar brojZadataka');
				assert.equal(data,null);
			})

			this.requests[0].respond(200, { 'Content-Type': 'text/json' }, dataJson);
		})				

	 })
	 describe('iscrtajZadatke()', function() {
	 	afterEach(function() {
	 		var div = document.getElementById("mocha");

	 		while(div.children.length != 0)
	 			div.removeChild(div.lastElementChild);

	 	});

		it('test - provjera da li se iscrtaju potrebni elementi',function () {
	 		var div = document.getElementById("mocha");

	 		var brojZadataka = 9;

	 		VjezbeAjax.iscrtajZadatke(div,brojZadataka);

	 		assert.equal(div.children.length, 1, "treba biti iscrtan 1 div");
	 		assert.equal(div.getElementsByTagName("div")[0].children.length, 9, "treba biti 9 input polja unutar diva");
	 	})	
	 })
	 
	 describe('iscrtajVjezbe()', function() {
	 	afterEach(function() {
	 		var div = document.getElementById("mocha");

	 		while(div.children.length != 0)
	 			div.removeChild(div.lastElementChild);

	 	});

		it('test - provjera da li se iscrtaju potrebni elementi',function () {
	 		var div = document.getElementById("mocha");

	 		var objekat = {brojVjezbi: 10, brojZadataka:[1,2,3,4,5,6,7,8,9,10]};

	 		VjezbeAjax.iscrtajVjezbe(div,objekat);

	 		var divVjezba1 = div.getElementsByTagName("div")[0];
	 		assert.equal(div.children.length, 10, "treba iscrtati 10 vjezbi");
	 		assert.equal(divVjezba1.children.length,1, "treba biti samo button VJEZBA 1");
	 	})	
	 })
})