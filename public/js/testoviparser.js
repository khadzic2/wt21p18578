var TestoviParser = (function () {

    const dajTacnost = function (string){
        try{
            var objekat = JSON.parse(string);
            var brojUkupnihTestova = objekat.tests.length;
            var brojUspjesnihTestova = objekat.passes.length;
            var procenatTacnosti = (brojUspjesnihTestova/brojUkupnihTestova)*100;
            procenatTacnosti = procenatTacnosti.toFixed(1);
            if(Number.isInteger(procenatTacnosti))
                procenatTacnosti = procenatTacnosti.toFixed(0);
            var listaGresaka = [];
            if(procenatTacnosti < 100){
                for (var i = 0; i < objekat.failures.length; i++) {
                    listaGresaka.push(objekat.failures[i].fullTitle);
                }
            }
            var rezultat = {"tacnost":procenatTacnosti + "%", "greske":listaGresaka}
            return rezultat;
        }catch(err){
            var rezultat = {"tacnost":0 + "%", "greske": ["Testovi se ne mogu izvrsiti"]};
            return rezultat;
        }
    }
    const porediRezultate = function(rezultat1, rezultat2){
        try{
            var objekatRez1 = JSON.parse(rezultat1);
            var objekatRez2 = JSON.parse(rezultat2);
            
            var tacnostRez1 = dajTacnost(rezultat1);
            var tacnostRez2 = dajTacnost(rezultat2);

            var neuspjesniTestoviRez1 = tacnostRez1.greske;
            var neuspjesniTestoviRez2 = tacnostRez2.greske;
            var uspjesniTestoviRez1 = [];
            var uspjesniTestoviRez2 = [];
            var testoviRez1 = [];
            var testoviRez2 = [];  

            for(var i = 0; i < objekatRez1.tests.length; i++){
                testoviRez1.push(objekatRez1.tests[i].fullTitle);
            }
            for(var i = 0; i < objekatRez2.tests.length; i++){
                testoviRez2.push(objekatRez2.tests[i].fullTitle);
            }
            for (var i = 0; i < objekatRez1.passes.length; i++) {
                uspjesniTestoviRez1.push(objekatRez1.passes[i].fullTitle);
            }
            for (var i = 0; i < objekatRez2.passes.length; i++) {
                uspjesniTestoviRez2.push(objekatRez2.passes[i].fullTitle);
            }

            var x = 0;
            var greskeRez = [];

            var isti = true;
            if(testoviRez1.length == testoviRez2.length){
                for(var testrez1 in testoviRez1){
                    var postoji = false;
                    for(var testrez2 in testoviRez2){
                        if(testoviRez1[testrez1] == testoviRez2[testrez2]){ postoji = true; break;}
                    }
                    if(!postoji){isti = false;  break;}
                }
            }

            if(isti){
                x = tacnostRez2.tacnost;
                greskeRez = neuspjesniTestoviRez2;
                greskeRez.sort();
            }else{
                var broj = 0;
                var greske1 = [];
                var greske2 = [];
                for(var test1 in neuspjesniTestoviRez1){
                    var postoji = false;
                    for(var test2 in testoviRez2){
                        if(neuspjesniTestoviRez1[test1] == testoviRez2[test2]) {postoji = true; break;}
                    }
                    if(!postoji) {broj++; greske1.push(neuspjesniTestoviRez1[test1]);}
                }
                x = (broj + neuspjesniTestoviRez2.length)/(broj + testoviRez2.length);
                x *= 100;
                x = x.toFixed(1);
                if(Number.isInteger(x))
                    x = x.toFixed(0);

                greske2 = neuspjesniTestoviRez2;
        
                greske1.sort();
                greske2.sort();
                for(var greska in greske1) greskeRez.push(greske1[greska]);
                for(var greska in greske2) greskeRez.push(greske2[greska]);        
            }

            var rezultat = {"promjena":x +"%", "greske":greskeRez};
            return rezultat;
        }catch(poruka){
            return {"promjena":0 + "%", "greske":["Testovi se ne mogu izvrsiti"]};
        }
    }
   return {
       dajTacnost: dajTacnost,
       porediRezultate: porediRezultate
   }
}());