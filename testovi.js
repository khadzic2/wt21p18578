const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
chai.should();
let assert = chai.assert;

const server = require('./index.js');
const Sequelize = require('sequelize');
const db = require('./db.js');

describe('Testovi za POST student',function() {
	it('test1',function (done) {
		chai.request(server).post('/student').set('content-type','application/json').send({ime:"test",prezime:"test",index:"11111",grupa:"test"})
		.end(function(error,res) {
			var obj = JSON.parse(res.text);
			assert.equal(obj.status,"Kreiran student!");
			res.should.have.status(200);
			done();
		})
	})

	it('test2',function (done) {
		chai.request(server).post('/student').set('content-type','application/json').send({ime:"test",prezime:"test",index:"11111",grupa:"test"})
		.end(function(error,res) {
			var obj = JSON.parse(res.text);
			assert.equal(obj.status,"Student sa indexom 11111 već postoji!");
			res.should.have.status(200);
			done();
		})
	})
})