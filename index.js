const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();
const url = require('url');
const db = require('./db.js');

app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname+"/public"));
app.use(express.static(__dirname+"/public/html/"));

app.get("/vjezbe", function (req, res) {
	db.vjezba.findAll({order: [['naziv', 'ASC']]}).then(function(data) {
		var brojVjezbi = 0;
		var brojZadataka = [];
		data.forEach(function(row) {
			brojVjezbi += 1;
			brojZadataka.push(row.brojZadataka);
		});

		var objekat = {brojVjezbi: brojVjezbi, brojZadataka: brojZadataka};
		res.end(JSON.stringify(objekat));
	});
});

app.post("/vjezbe", function(req,res){
	var brojVjezbi = req.body.brojVjezbi;
	var brojZadataka = req.body.brojZadataka;
	var status = "error";
	var data = "Pogrešan parametar ";
	var greska = false;

	if(brojVjezbi < 1 || brojVjezbi > 15){
		data += "brojVjezbi";
		greska = true;
	}

	for (var i = 0; i < brojZadataka.length; i++) {
		if(brojZadataka[i] < 0 || brojZadataka[i] > 10){
			if(greska) data += ",";

			data += "z" + i;
			greska = true;
		}
	}

	if(brojZadataka.length != brojVjezbi){
		if(greska) data += ",";

		greska = true;
		data += "brojZadataka";
	}

	var objekat = {};
	if(greska) objekat = {status: status, data: data};
	else objekat = {brojVjezbi: brojVjezbi, brojZadataka: brojZadataka};

	if(!greska){
		db.zadatak.destroy({cascade:true,where:{},restarIdentity:true}).then(function(){
			db.vjezba.destroy({cascade:true,where:{},restarIdentity:true}).then(function(){
				var vjezbeListaPromisea = [];
				for (var i = 0; i < brojVjezbi; i++) {
					vjezbeListaPromisea.push(db.vjezba.create({naziv:'Vjezba ' + (i+1).toString(),brojZadataka:brojZadataka[i]}));
				}

				Promise.all(vjezbeListaPromisea).then(function(vjezbe) {
					vjezbe.forEach(function(vjezba) {
						var zadaciListaPromisea = [];

						for (var i = 0; i < vjezba.brojZadataka; i++) {
							zadaciListaPromisea.push(db.zadatak.create({naziv:'Zadatak ' + (i+1).toString()}));
						}

						Promise.all(zadaciListaPromisea).then(function(zadaci) {
							vjezba.setZadaciVjezbe(zadaci);
						})
					})
				})    		
			})				
		})
	}
	res.end(JSON.stringify(objekat));
});

app.post("/student",function(req,res) {
	var ime = req.body.ime;
	var prezime = req.body.prezime;
	var index = req.body.index;
	var grupa = req.body.grupa;
	var objekat={status:"Podaci nisu validni"};
	if(ime.length==0 || prezime.length==0 || index.length==0 || grupa.length==0)
		res.end(JSON.stringify(objekat));
	else
		db.student.findOne({where:{index:index}}).then(function(student) {
			if(student != null){
				objekat = {status:'Student sa indexom ' + index + ' već postoji!'};
				res.end(JSON.stringify(objekat));
			}else{
				db.grupa.findOrCreate({where:{naziv:grupa}}).then(function(g){
					g = g[0];
					db.student.create({ime:ime,prezime:prezime,index:index}).then(function(s) {
						db.student.findAll({where:{grupaId:g.id}}).then(function(studenti) {
							studenti.push(s);
							g.setStudentiGrupe(studenti);
							objekat = {status:'Kreiran student!'};
							res.end(JSON.stringify(objekat));
						})
					});
				});
			}	
		});
});

app.put("/student/:index",function(req,res) {
	var index = req.params.index;
	var grupa = req.body.grupa;
	var objekat={status:"Podaci nisu validni"};
	if(grupa.length==0 || index.length==0)
		res.end(JSON.stringify(objekat));
	else
		db.student.findOne({where:{index:index}}).then(function(student) {
			if(student == null){
				objekat = {status:'Student sa indexom ' + index + ' ne postoji'};
				res.json(objekat);
				return;
			}else{
				db.grupa.findOrCreate({where:{naziv:grupa}}).then(function(g){
					g = g[0];
					student.update({grupaId:g.id}).then(function(s) {
						objekat = {status:'Promjenjena grupa studentu ' + index};
						res.end(JSON.stringify(objekat));
					});
				});
			}	
		});
});

app.post("/batch/student",function(req,res) {
	if(req.body.length==0)
		res.end(JSON.stringify({status:"Prazni podaci"}))
	else{
		var lista = req.body.split('\n');
		
		var redovi = [];
		var studentiPromisea = [];

		lista.forEach(function(red) {
			var podaci = red.split(',');
			var validniPodaci = podaci.filter(pod=>pod.trim()!="").length == 4;
			if(podaci.length == 4 && validniPodaci){
				redovi.push(red);
				studentiPromisea.push(db.student.findOne({where:{index:podaci[2]}}));
			}
		})	

		var postojeciStudentiIndexi = [];
		var n = 0;
		Promise.all(studentiPromisea).then(function(students) {
			var kreiratiStudente = [];
			var redoviStudenti = [];
			for(i=0; i<redovi.length; i++){
				var podaci = redovi[i].split(',');
				var postojiIndex = false;

				for(j=0; j<i; j++){
					var indexred = redovi[j].split(',')[2];
					if(indexred == podaci[2]) postojiIndex = true;
				}

				if(students[i] == null && !postojiIndex){
					kreiratiStudente.push(db.student.create({ime:podaci[0],prezime:podaci[1],index:podaci[2]}));	
					redoviStudenti.push(redovi[i]);			
					n = n + 1;
				}else{
					postojeciStudentiIndexi.push(podaci[2]);
				}
			}

			Promise.all(kreiratiStudente).then(function(kreiraniStudenti) {
				var i = 0;
				if(kreiraniStudenti.length != 0){
					kreiraniStudenti.forEach(function(student) {
						i = i + 1;
						var studentred = redoviStudenti.filter(red=> {
							var pom = red.split(',');
							return pom[0]==student.ime && pom[1]==student.prezime && pom[2]==student.index;
						})[0];	

						var nazivgrupe = studentred.split(',')[3];

						db.grupa.findOrCreate({where:{naziv:nazivgrupe}}).then(function(g) {
							g = g[0];
							student.update({grupaId:g.id}).then(function(s) {
								if(i == n){
									var objekat = {};
									if(postojeciStudentiIndexi.length > 0) objekat =  {status:"Dodano " + n + " studenata, a studenti " + postojeciStudentiIndexi.join(',') + " već postoje!"};
									else objekat = {status:"Dodano " + n + " studenata!"};
									res.end(JSON.stringify(objekat));
								}
							});					
						})
					})
				}else{
					if(postojeciStudentiIndexi.length > 0) objekat =  {status:"Dodano " + n + " studenata, a studenti " + postojeciStudentiIndexi.join(',') + " već postoje!"};
					else objekat = {status:"Dodano " + n + " studenata!"};
					res.end(JSON.stringify(objekat));
				}
			})		
		})
	}		
})
let server = app.listen(3000);
module.exports = server;